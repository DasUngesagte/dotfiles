#!/bin/bash

gatewayIP=$(netstat -nr | awk '$1 == "0.0.0.0"{print$2}')

if [  "$gatewayIP" = "192.168.2.1" ]
then
	mount -t cifs -o credentials=/home/lars/.smbcredentials //192.168.2.104/NAS /media/NAS
fi

