#!/usr/bin/python3
import os
import subprocess
from urllib.parse import unquote

USE_PYWAL = False
ALBUM_WALLPAPER_PATH = ""
CURRENT_PATH = ""
MUSIC_DIR = "/media/Daten/Music"


def get_metadata():

    global MUSIC_DIR, ALBUM_WALLPAPER_PATH, CURRENT_PATH

    #album = subprocess.run("playerctl metadata xesam:album", shell=True, stdout=subprocess.PIPE)
    #CURRENT_ALBUM = album.stdout.decode('utf-8').splitlines()[0]

    path = subprocess.run("playerctl -p quodlibet metadata xesam:url", shell=True, stdout=subprocess.PIPE)
    CURRENT_PATH = path.stdout.decode('utf-8').splitlines()[0]
    if("file://" in CURRENT_PATH):
        CURRENT_PATH = unquote(CURRENT_PATH)
        CURRENT_PATH = CURRENT_PATH.replace("file://", "")
    else:
        CURRENT_PATH = MUSIC_DIR + "/" + path.stdout.decode('utf-8').splitlines()[0]
    CURRENT_PATH = "/".join(CURRENT_PATH.split("/")[:-1]) + "/"


def check_existence():

    global ALBUM_WALLPAPER_PATH, CURRENT_PATH

    ALBUM_WALLPAPER_PATH = CURRENT_PATH + "AlbumWallpaper.jpg"
    exists = os.path.isfile(ALBUM_WALLPAPER_PATH)
    return exists


def create_cover():

    global ALBUM_WALLPAPER_PATH, CURRENT_PATH

    try:
        album_cover = subprocess.run("find \"" + CURRENT_PATH + "\" -type d -exec find {} -maxdepth 1 -type f -iregex \".*/.*\(cover\|folder\|artwork\|front\).*[.]\(jpe?g\|png\|gif\|bmp\)\" \;", shell=True, stdout=subprocess.PIPE)
        album_cover = album_cover.stdout.decode('utf-8').splitlines()[0]
        
        # Resize inner album cover with border:
        subprocess.run("convert \"" + album_cover + "\" -resize '1080>' -interpolate integer -bordercolor white -border 5 \"/tmp/cover.jpg\"", shell=True, stdout=subprocess.PIPE)
        # Resize outer cover and blur it:
        subprocess.run("convert \"" + album_cover + "\" -resize 2560x2560^ -interpolate Bilinear -gravity center -extent 2560x1440 \"/tmp/cover_back.ppm\"", shell=True, stdout=subprocess.PIPE)
        subprocess.run("GaussianBlurPPM \"/tmp/cover_back.ppm\" \"/tmp/cover_blurred.ppm\" 35.0", shell=True, stdout=subprocess.PIPE)
        # Compose the two images:
        subprocess.run("composite -compose atop -interpolate Bilinear -gravity Center \"/tmp/cover.jpg\" \"/tmp/cover_blurred.ppm\" \"" + ALBUM_WALLPAPER_PATH + "\"" , shell=True, stdout=subprocess.PIPE)
    
        os.remove("/tmp/cover.jpg")
        os.remove("/tmp/cover_back.ppm")
        os.remove("/tmp/cover_blurred.ppm")

        return True
    except:
        return False

def main():
    get_metadata()

    art_exists = check_existence()
    if(not art_exists):
        art_exists = create_cover()

    # Copy or remove cover from temp:
    if(art_exists):
        if(USE_PYWAL):
            subprocess.run("wal -i \"" + ALBUM_WALLPAPER_PATH + "\" -e", shell=True, stdout=subprocess.PIPE)
        else:
            subprocess.run("feh --bg-fill \"" + ALBUM_WALLPAPER_PATH + "\"", shell=True, stdout=subprocess.PIPE)
    else:
        if(USE_PYWAL):
            subprocess.run("wal -i ~/Pictures/Wallpaper/ -e", shell=True, stdout=subprocess.PIPE)
        else:
            subprocess.run("feh --randomize --bg-fill ~/Pictures/Wallpaper/*", shell=True, stdout=subprocess.PIPE)


if __name__ == "__main__":
    main()

