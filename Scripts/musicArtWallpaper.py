#!/usr/bin/python3
import argparse
import os
import subprocess
from urllib.parse import unquote
from gi.repository import GLib
import gi

gi.require_version('Playerctl', '2.0')
from gi.repository import Playerctl


MUSIC_DIR = "/media/Daten/Music"
BACKUP_DIR = "/home/lars/Pictures/Wallpaper"
SPOTIFY_ART_DIR = "/home/lars/Pictures/Spotify-Cover"

ALBUM_WALLPAPER_PATH = ""
CURRENT_PATH = ""
LAST_ALBUM = ""
IS_SPOTIFY = False
USE_SWAY = False    # read via --sway option

SUPPORTED_PLAYERS =  ['spotify', 'quodlibet']


def get_metadata(metadata):

    global MUSIC_DIR, ALBUM_WALLPAPER_PATH, CURRENT_PATH, IS_SPOTIFY, LAST_ALBUM

    keys = metadata.keys()
    if 'mpris:artUrl' in keys and 'xesam:album' in keys:
        current_album = metadata['xesam:album']
        path = metadata['mpris:artUrl']

    #print(path)
    CURRENT_PATH = path
    
    if("file://" in CURRENT_PATH):
        CURRENT_PATH = unquote(CURRENT_PATH)
        CURRENT_PATH = CURRENT_PATH.replace("file://", "")
        CURRENT_PATH = "/".join(CURRENT_PATH.split("/")[:-1]) + "/"
    elif("https://" in CURRENT_PATH):
        IS_SPOTIFY = True
    else:
        CURRENT_PATH = MUSIC_DIR + "/" + path
        CURRENT_PATH = "/".join(CURRENT_PATH.split("/")[:-1]) + "/"

    #print("Aktuelles Album:" + current_album)
    return current_album


def check_existence():

    global ALBUM_WALLPAPER_PATH, CURRENT_PATH, IS_SPOTIFY, SPOTIFY_ART_DIR

    if(IS_SPOTIFY):
        ALBUM_WALLPAPER_PATH = SPOTIFY_ART_DIR + "/" + CURRENT_PATH.split("/")[-1:][0] + ".jpg"
    else:
        ALBUM_WALLPAPER_PATH = CURRENT_PATH + "AlbumWallpaper.jpg"

    exists = os.path.isfile(ALBUM_WALLPAPER_PATH)
    return exists


def create_cover():

    global ALBUM_WALLPAPER_PATH, CURRENT_PATH, IS_SPOTIFY

    try:
        if(IS_SPOTIFY):
            album_cover = "/tmp/" + CURRENT_PATH.split("/")[-1:][0] + ".jpg"     # Add spotify id
            subprocess.run("curl -fs " + CURRENT_PATH + " --output " + album_cover, shell=True, stdout=subprocess.PIPE)
            print("Downloading " + album_cover)

        else:
            album_cover = subprocess.run("find \"" + CURRENT_PATH + "\" -type d -exec find {} -maxdepth 1 -type f -iregex \".*/.*\(cover\|folder\|artwork\|front\).*[.]\(jpe?g\|png\|gif\|bmp\)\" \;", shell=True, stdout=subprocess.PIPE)
            album_cover = album_cover.stdout.decode('utf-8').splitlines()[0]

        # Resize inner album cover:
        subprocess.run("convert \"" + album_cover + "\" -adaptive-resize '1080' \"/tmp/cover.jpg\"", shell=True, stdout=subprocess.PIPE)
        # Resize outer cover and blur it:
        subprocess.run("convert \"" + album_cover + "\" -resize 2560x2560^ -interpolate Bilinear -gravity center -extent 2560x1440 \"/tmp/cover_back.ppm\"", shell=True, stdout=subprocess.PIPE)
        subprocess.run("convert \"/tmp/cover_back.ppm\" -blur 0x30 \"/tmp/cover_blurred.ppm\" ", shell=True, stdout=subprocess.PIPE)
        # Compose the two images:
        subprocess.run("composite -compose atop -interpolate Bilinear -gravity Center /tmp/cover.jpg /tmp/cover_blurred.ppm \"" + ALBUM_WALLPAPER_PATH + "\"" , shell=True, stdout=subprocess.PIPE)

        retval = True

    except Exception as ex:
        print(ex)
        retval = False
    finally:
        try:
            os.remove("/tmp/cover.jpg")
        except OSError as ex:
            print(ex)
        try:
            os.remove("/tmp/cover_back.ppm")
        except OSError as ex:
            print(ex)
        try:
            os.remove("/tmp/cover_blurred.ppm")
        except OSError as ex:
            print(ex)
        if(IS_SPOTIFY):
            try:
                os.remove(album_cover)
            except OSError as ex:
                print(ex)
            #print("Deleting " + album_cover)

    return retval


def on_track_change(player, metadata, manager):

    global ALBUM_WALLPAPER_PATH, IS_SPOTIFY, LAST_ALBUM, BACKUP_DIR, USE_SWAY

    IS_SPOTIFY = False

    current_album = get_metadata(metadata)
    #print("Last Album:" + LAST_ALBUM)
    #print("Current Album:" + current_album)

    if(current_album != LAST_ALBUM):
        art_exists = check_existence()
        if(not art_exists):
            art_exists = create_cover()
        if(art_exists):
            if USE_SWAY:
                subprocess.run("swww img \"" + ALBUM_WALLPAPER_PATH + "\" --transition-type simple --transition-speed 2 --transition-step 20 --transition-fps 30", shell=True, stdout=subprocess.PIPE)
            else:
                subprocess.run("feh --bg-fill \"" + ALBUM_WALLPAPER_PATH + "\"", shell=True, stdout=subprocess.PIPE)
        else:
            if USE_SWAY:
                pass
            else: 
                subprocess.run("feh --randomize --bg-fill " + BACKUP_DIR + "/*", shell=True, stdout=subprocess.PIPE)

        LAST_ALBUM = current_album
    else:
        pass


def init_player(manager, name):

    global SUPPORTED_PLAYERS
    
    if name.name in SUPPORTED_PLAYERS:
        player = Playerctl.Player.new_from_name(name)
        player.connect('metadata', on_track_change, manager)
        manager.manage_player(player)


def on_name_appeared(manager, name):
    init_player(manager, name)


def main():

    global USE_SWAY

    parser = argparse.ArgumentParser(description='Start wallpaper daemon')
    parser.add_argument('--sway', help='Using swww for wallpaper', action='store_true')
    args = parser.parse_args()
    
    USE_SWAY = args.sway

    manager = Playerctl.PlayerManager()

    manager.connect('name-appeared', on_name_appeared)

    for name in manager.props.player_names:
        init_player(manager, name)

    main = GLib.MainLoop()
    main.run()
    
if __name__ == "__main__":
    main()

