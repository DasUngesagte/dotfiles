#!/bin/bash

i3-msg workspace 8 &&
sleep 0.1 &&
i3-msg "workspace 7; append_layout /home/lars/.config/i3/quadrat.json" &
streamlink -p vlc "https://d3jx1q5huh61rb.cloudfront.net/out/v1/4a177604a05d421ba4bef02f80d34f04/index.m3u8" 720p &
streamlink -p vlc "https://d2ygd20he8ebio.cloudfront.net/out/v1/402b39a78354468abcc089e989143c6e/index.m3u8" 720p &
streamlink -p vlc "https://d25fzu9ozfnso0.cloudfront.net/out/v1/c6f8f8300cdd4c14bd4b4d4e0b180ba3/index.m3u8" 720p &
streamlink -p vlc "https://www.youtube.com/watch?v=xstzLlgfNA4" 720p &
wait
