#!/bin/sh

quelle=/home/lars/
ziel=/media/Daten/HOMEBACKUP/
heute=$(date +%Y-%m-%d)

rsync -avR --delete "${quelle}"  "${ziel}${heute}/" --link-dest="${ziel}last/"
ln -nsf "${ziel}${heute}" "${ziel}last"

exit 0
