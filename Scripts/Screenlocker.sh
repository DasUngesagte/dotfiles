#!/bin/sh

sleep 0.2

pgrep sway  # Check for running sway

if [ $? -eq 0 ]
then
    grim -o DP-1 /tmp/screenDP1.jpg -t jpeg
    grim -o DP-3 /tmp/screenDP3.jpg -t jpeg

    convert /tmp/screenDP1.jpg -blur 0x30 /tmp/screenDP1.jpg
    convert /tmp/screenDP3.jpg -blur 0x30 /tmp/screenDP3.jpg
    swaylock -u -i DP-1:/tmp/screenDP1.jpg -i DP-3:/tmp/screenDP3.jpg

    rm /tmp/screenDP3.jpg /tmp/screenDP1.jpg
else
    maim /tmp/screen.jpg
    convert /tmp/screen.jpg -blur 0x30 /tmp/screen.jpg
    i3lock -u -i /tmp/screen.jpg

    rm /tmp/screen.jpg
fi
