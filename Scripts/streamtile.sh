#!/bin/bash

i3-msg workspace 7 &&
sleep 0.1 &&
i3-msg "workspace 7; append_layout /home/lars/.config/i3/quadrat.json" &
streamlink -p vlc "https://www.youtube.com/watch?v=Yn5Gyc5s7GE" best & #2
streamlink -p vlc "https://www.youtube.com/watch?v=Kch6NbE8L60" best & #7
streamlink -p vlc "https://www.youtube.com/watch?v=f5SwyfqTUiM" best & #6
streamlink -p vlc "https://www.youtube.com/watch?v=BRYmUk8b0MU" best & #1
wait
