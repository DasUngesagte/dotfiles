#!/bin/sh

# Setting this, so the repo does not need to be given on the commandline:
# export BORG_REPO=lars@pi3.local:/media/Backup/Lars
export BORG_REPO=/run/media/lars/Backup/Lars

# Setting this, so you won't be asked for your repository passphrase:
export BORG_PASSPHRASE='Sicheres Backup'
# or this to ask an external program to supply the passphrase:
#export BORG_PASSCOMMAND='pass show backup'

# some helpers and error handling:
info() { printf "\n%s %s\n\n" "$( date )" "$*" >&2; }
trap 'echo $( date ) Backup interrupted >&2; exit 2' INT TERM

info "Starting backup"
notify-send -u normal "Starting backup"
# Backup the most important directories into an archive named after
# the machine this script is currently running on:

borg create                          \
    --filter AME                     \
    --stats                          \
    --show-rc                        \
    --compression lz4                \
    --exclude-caches                 \
    --exclude '/home/*/.cache/*'     \
    --exclude '/var/cache/*'         \
    --exclude '/var/tmp/*'           \
    --exclude '/media/Daten/Games'   \
    --exclude '/media/Daten/Games -*'\
    --exclude '/media/Daten/WpSystem'\
    --exclude '/media/Daten/WindowsApps'\
                                     \
    ::'{hostname}-{now}'             \
    /home/lars                       \
    /media/Daten                     \
    /media/Windows/Users/Lars        \

backup_exit=$?

info "Pruning repository"

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machines' archives also:

borg prune                          \
    --list                          \
    --prefix '{hostname}-'          \
    --show-rc                       \
    --keep-daily    7               \
    --keep-weekly   4               \
    --keep-monthly  6               \

prune_exit=$?

# use highest exit code as global exit code
global_exit=$(( backup_exit > prune_exit ? backup_exit : prune_exit ))

if [ ${global_exit} -eq 1 ];
then
    info "Backup and/or Prune finished with a warning"
    notify-send -u critical "Backup ended with warning"
fi

if [ ${global_exit} -gt 1 ];
then
    info "Backup and/or Prune finished with an error"
    notify-send -u critical "Backup failed"
fi

notify-send -u normal "Backup finished"

#exit ${global_exit}
$SHELL
