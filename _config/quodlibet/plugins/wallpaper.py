#!/usr/bin/python3
from quodlibet.plugins.events import EventPlugin
import os
from multiprocessing import Process

def main():
    os.system('python3 /home/lars/Scripts/musicArtWallpaper.py')

class Wallpaper(EventPlugin):
    PLUGIN_ID = "wallpaper"
    PLUGIN_NAME = "Wallpaper"

    def plugin_on_song_started(self, song):
        if song is not None:
            p1 = Process(target=main)
            p1.start()
