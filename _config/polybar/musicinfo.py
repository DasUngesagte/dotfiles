#!/usr/bin/python3
import subprocess

# Globals:
ARTIST = "Unavailable"
TITLE = "Unavailable"
IS_PAUSED = True
NO_PLAYER = False


def get_metadata():

    global TITLE, ARTIST, SYMBOL, IS_PAUSED, NO_PLAYER

    player_list = subprocess.run("playerctl -l", shell=True, stdout=subprocess.PIPE)
    player_list = player_list.stdout.decode('utf-8').splitlines()

    try:
        player_for_data = player_list[0]  # Take a fallback when every player is paused#
        for player in player_list:

            if "vlc" in player or "firefox" in player:
                continue

            is_playing = subprocess.run("playerctl -p " + player + " status", shell=True, stdout=subprocess.PIPE)
            is_playing = is_playing.stdout.decode('utf-8').rstrip()

            if(is_playing == "Playing"):
                player_for_data = player
                IS_PAUSED = False
                break;

        result = subprocess.run("playerctl -p " + player + " metadata xesam:artist", shell=True, stdout=subprocess.PIPE)
        ARTIST = result.stdout.decode('utf-8')

        result = subprocess.run("playerctl -p " + player + " metadata xesam:title", shell=True, stdout=subprocess.PIPE)
        TITLE = result.stdout.decode('utf-8')

    except:
        IS_PAUSED = False
        NO_PLAYER = True


if __name__ == "__main__":

    get_metadata()
    if(IS_PAUSED):
        print(" Paused")
    elif(NO_PLAYER):
        print("")
    elif(not IS_PAUSED):
        print(" {} -  {}".format(TITLE, ARTIST).replace('\n', ' ').replace('\r', ''))
