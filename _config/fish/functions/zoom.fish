function zoom
	xrandr --output DP-0 --mode 1920x1080
	xrandr --output HDMI-1 --mode 1920x1080 --scale 1x1 --same-as DP-0
end
