function scan
    scanimage --device "airscan:e0:Canon TR8500 series" --resolution 600 --format=png --output-file $argv.png --progress 
end

#All options specific to device `airscan:e0:Canon TR8500 series':
#  Standard:
#    --resolution 75|100|150|200|300|600|1200dpi [300]
#        Sets the resolution of the scanned image.
#    --mode Color|Gray [Color]
#        Selects the scan mode (e.g., lineart, monochrome, or color).
#    --source Flatbed|ADF [Flatbed]
#        Selects the scan source (such as a document-feeder).
#  Geometry:
#    -l 0..215.9mm [0]
#        Top-left x position of scan area.
#    -t 0..297.011mm [0]
#        Top-left y position of scan area.
#    -x 0..215.9mm [215.9]
#        Width of scan-area.
#    -y 0..297.011mm [297.011]
#        Height of scan-area.
#  Enhancement:
#    --brightness -100..100% (in steps of 1) [0]
#        Controls the brightness of the acquired image.
#    --contrast -100..100% (in steps of 1) [0]
#        Controls the contrast of the acquired image.
#    --shadow 0..100% (in steps of 1) [0]
#        Selects what radiance level should be considered "black".
#    --highlight 0..100% (in steps of 1) [100]
#        Selects what radiance level should be considered "white".
#    --analog-gamma 0.0999908..4 [1]
#        Analog gamma-correction
#    --negative[=(yes|no)] [no]
#        Swap black and white
