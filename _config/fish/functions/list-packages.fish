function list-packages
	paste (yay -Qie | grep 'Size' | awk '{ print $4$5;}' | psub) (yay -Qie | grep 'Name' | awk '{print $3}'| psub) | sort -h | column -t
end
