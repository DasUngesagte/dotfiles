function fish_prompt --description 'Write out the prompt'
	set -l last_status $status

	if not set -q __fish_git_prompt_show_informative_status
	    	set -g __fish_git_prompt_show_informative_status 1
  	end
  	if not set -q __fish_git_prompt_color_branch
    		set -g __fish_git_prompt_color_branch brmagenta
  	end
  	if not set -q __fish_git_prompt_showupstream
    		set -g __fish_git_prompt_showupstream "informative"
  	end
  	if not set -q __fish_git_prompt_showdirtystate
    		set -g __fish_git_prompt_showdirtystate "yes"
  	end
  	if not set -q __fish_git_prompt_color_stagedstate
    		set -g __fish_git_prompt_color_stagedstate yellow
  	end
  	if not set -q __fish_git_prompt_color_invalidstate
    		set -g __fish_git_prompt_color_invalidstate red
  	end
  	if not set -q __fish_git_prompt_color_cleanstate
    		set -g __fish_git_prompt_color_cleanstate brgreen
  	end

	if test -z $WINDOW
        	printf '%s%s@%s%s %s%s%s%s %s $ ' (set_color --bold brgreen) (whoami)  (prompt_hostname) (set_color blue) (prompt_pwd) (set_color --bold red) (__fish_git_prompt)
    	else
       		printf '%s%s@%s%s%s(%s)%s%s%s> ' (set_color yellow) (whoami) (set_color purple) (prompt_hostname) (set_color white) (echo $WINDOW) (set_color $fish_color_cwd) (prompt_pwd) (set_color normal)
    end

	if not test $last_status -eq 0
    		set_color $fish_color_error
 	end
  	set_color normal
end
