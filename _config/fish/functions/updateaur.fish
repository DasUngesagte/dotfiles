function updateaur
    set_color --bold red
    echo "Checking AUR ..."
    set_color normal
    auracle sync
    set_color --bold red
    auracle -C ~/AUR/ download (auracle sync --quiet)
    set_color normal
end
