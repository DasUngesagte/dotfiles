{
    "output": "DP-1",
    // "layer": "top", // Waybar at top layer
    // "position": "bottom", // Waybar position (top|bottom|left|right)
    "height": 25, // Waybar height (to be removed for auto height)
    // "width": 1280, // Waybar width
    "spacing": 1, // Gaps between modules (4px)
    // Choose the order of the modules
    "modules-left": ["sway/workspaces", "sway/mode", "custom/scratchpad", "custom/gsimplecal", "sway/window"],
    "modules-center": ["clock"],
    "modules-right": ["custom/music", "custom/gputemp", "custom/watertemp", "custom/mousebat", "pulseaudio", "custom/updates", "custom/powermenu", "tray"],
    // Modules configuration
    "sway/workspaces": {
         "disable-scroll": true,
         "all-outputs": true,
         "format": "{name} > {icon}",
         "format-icons": {
             "1": "",
             "2": "",
             "3": "",
             "5": "",
             "6": "",
             "urgent": "",
             "default": ""
         }
    },
    "keyboard-state": {
        "numlock": true,
        "capslock": true,
        "format": "{name} {icon}",
        "format-icons": {
            "locked": "",
            "unlocked": ""
        }
    },
    "sway/mode": {
        "format": "{}"
    },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "tray": {
        "icon-size": 16,
        "spacing": 10
    },
    "clock": {
        // "timezone": "America/New_York",
        "format": "{:%d. %B - %H:%M:%S}",
        "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
        //"format-alt": "{:%Y-%m-%d}"
        "format-alt": "{:%A, %d. %B %Y}",
        "interval": 1,
        "on-click-right": "gsimplecal"
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "<span color='#A6E22A'>{icon}</span> {volume}%   {format_source}",
        "format-bluetooth": "{volume}% <span color='#A6E22A'>{icon}</span>   {format_source}",
        "format-bluetooth-muted": "<span color='#A6E22A'></span>   {format_source}",
        "format-muted": "<span color='#A6E22A'></span>   {volume}%   {format_source}",
        "format-source": "<span color='#A6E22A'></span> {volume}%",
        "format-source-muted": "<span color='#A6E22A'></span>",
        "format-icons": {
            "default": ["", "", ""]
        },
        "on-click": "pavucontrol"
    },
    "custom/music": {
        "format": "<span color='#A6E22A'>{icon}</span> {}",
        "return-type": "text",
        "max-length": 80,
        "format-icons": {
            "default": "🎜"
        },
        "on-click": "playerctl play-pause",
        "escape": true,
        "interval": 1,
        "exec": "/usr/bin/python3 $HOME/.config/polybar/musicinfo.py" // Script in resources folder
    },
    "custom/updates": {
        "format": "<span color='#A6E22A'>{icon}</span> {}",
        "return-type": "text",
        "max-length": 40,
        "format-icons": {
            "default": ""
        },
        "escape": true,
        "interval": 1800,
        "exec": "checkupdates | wc -l" // Script in resources folder
    },
    "custom/envsensor": {
        "format": "<span color='#A6E22A'>{icon}</span> {}",
        "return-type": "text",
        "max-length": 40,
        "format-icons": {
            "default": ""
        },
        "escape": true,
        "interval": 122,
        "exec": "/usr/bin/python3 $HOME/.config/polybar/RPiTempClient.py" // Script in resources folder
    },
    "custom/watertemp": {
        "format": "<span color='#A6E22A'>{icon}</span> {}",
        "return-type": "text",
        "max-length": 17,
        "format-icons": {
            "default": ""
        },
        "escape": true,
        "interval": 30,
        "exec": "echo $(sensors | grep \"Coolant temp:\" | cut -d+  -f2) $(sensors | grep \"Fan speed:\" | awk '{print $3, \"RPM\"}') | paste - -"
    },
    "custom/gputemp": {
        "format": "<span color='#A6E22A'>{icon}</span>  {}",
        "return-type": "text",
        "max-length": 40,
        "format-icons": {
            "default": ""
        },
        "escape": true,
        "interval": 30,
        "exec": "echo $(sensors | grep \"junction:\" | cut -d' ' -f6) $(sensors | grep \"mem:\" | cut -d' ' -f11)| paste - - | sed 's/+//g'"
    },
    "custom/mousebat": {
        "format": "<span color='#A6E22A'>{icon}</span> {}",
        "return-type": "text",
        "max-length": 6,
        "format-icons": {
            "default": ""
        },
        "escape": true,
        "interval": 600,
        "exec": "echo $(rogdrv-config sleep | grep \"Charge:\" | cut -d' ' -f2 -s)"
    },
    "custom/powermenu": {
        "format": "<span color='#A6E22A'>{icon}</span>",
        "return-type": "none",
        "max-length": 1,
        "format-icons": {
            "default": ""
        },
        "on-click": "$HOME/.config/rofi/scripts/powermenu" // Script in resources folder
    },
    "custom/scratchpad": {
        "format": "<span color='#89BDFF'>{icon}</span>",
        "return-type": "none",
        "max-length": 1,
        "format-icons": {
            "default": ""
        },
        "on-click": "swaymsg '[class=.*] scratchpad show'"
    },
    "custom/gsimplecal": {
        "format": "<span color='#89BDFF'>{icon}</span>",
        "return-type": "none",
        "max-length": 1,
        "format-icons": {
            "default": ""
        },
        "on-click": "gsimplecal"
    }
}

